<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserStatus extends Model
{
    protected $table = "user_status";
    protected $primaryKey = "id";
    protected $fillable = ['nama'];

    public function users()
    {
        return $this->hasMany('App\User');
    }
}
