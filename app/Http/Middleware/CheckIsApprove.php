<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class CheckIsApprove
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (Auth::check())
        {
            if (Auth::user()->user_status_id != '2')
            {
                Auth::logout();
                return redirect()->to('/')->with('warning', 'your account status is not approve');
            }
        }
        return $next($request);
    }
}
