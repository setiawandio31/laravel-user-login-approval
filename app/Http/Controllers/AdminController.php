<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;

class AdminController extends Controller
{
    public function __construct()
    {
      $this->middleware('auth');
    }

    public function index()
    {
        $users = User::where('user_status_id', 1)->get();
        if (request()->user()->hasRole('admin')) {
            return view('admin.index', ['users' => $users]);
        } else {
            return redirect('/');
        } 
    }

    public function approveUser(Request $request, $id)
    {
        $user = User::find($id);
        $user->user_status_id = 2;
        $user->save();

        return redirect()->route('admin');
    }

    public function rejectUser(Request $request, $id)
    {
        $user = User::find($id);
        $user->user_status_id = 3;
        $user->save();

        return redirect()->route('admin');
    }
}
