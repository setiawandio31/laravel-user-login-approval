@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Halaman ADMIN</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    <h3>User Approval</h3>

                    <table class="table table-bordered">
                        <thead>
                            <tr>
                                <th scope="col">Nama</th>
                                <th scope="col">Email</th>
                                <th scope="col">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach( $users as $user )
                                <tr>
                                    <td>{{ $user->name }}</td>
                                    <td>{{ $user->email }}</td>
                                    <td>
                                        <form style="display: inline;" action="{{ route('approveUser', ['id' => $user->id ]) }}" method="POST">
                                        @csrf
                                        @method('PUT')
                                            <button class="btn btn-primary btn-sm" type="submit">Approve</button>
                                        </form>
                                        <form style="display: inline;" action="{{ route('rejectUser', ['id' => $user->id ]) }}" method="POST">
                                        @csrf
                                        @method('PUT')
                                            <button class="btn btn-danger btn-sm" type="submit">Reject</button>
                                        </form>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection