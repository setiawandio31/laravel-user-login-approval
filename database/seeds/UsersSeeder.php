<?php

use Illuminate\Database\Seeder;

class UsersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'name' => 'admin', 
            'email' => 'setiawandio31@gmail.com',
            'email_verified_at' => '2020-08-21 08:48:46',
            'password' => Hash::make("admin"),
            'user_status_id' => 2
        ]);

        DB::table('role_users')->insert([
            "user_id" => 1,
            "role_id" => 2
        ]);
    }
}
