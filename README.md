<h1>Run This Project</h1>

## Tools
1. PHP
2. Web Server ( Apache )
3. DBMS ( MySQL )
4. Node JS

## Step
1. Composer Update
2. npm install && npm run dev
3. create a database with the name laravel
4. php artisan migrate --seed
5. php artisan serve

## User Login Admin
Email : setiawandio31@gmail.com
<br/>
Password : admin

# SSO
Single Sign On adalah mekanisme autentikasi untuk dapat mengakses beberapa situs atau aplikasi hanya dengan satu kali login saja. contohnya login ke beberapa aplikasi hanya dengan menggunakan akun google