<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
Auth::routes(['verify' => true]);
Route::get('/home', 'HomeController@index')->name('home');

Route::group(['middleware' => 'checkIsApprove'], function () {
    Route::get('/admin', 'AdminController@index')->name('admin');
    Route::put('/admin/approveUser/{id}', 'AdminController@approveUser')->name('approveUser');
    Route::put('/admin/rejectUser/{id}', 'AdminController@rejectUser')->name('rejectUser');
    
    Route::get('/user', 'UserController@index');
});
